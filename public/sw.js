importScripts("/src/js/idb.js");
importScripts("/src/js/utility.js");

var CACHE_STATIC_NAME = "static-v1";
var CACHE_DYNAMIC_NAME = "dynamic-v1";
var STATIC_FILES = [
  "/",
  "/index.html",
  "/offline.html",
  "/src/js/app.js",
  "/src/js/feed.js",
  "/src/js/idb.js",
  "/src/js/promise.js",
  "/src/js/fetch.js",
  "/src/js/material.min.js",
  "/src/css/app.css",
  "/src/css/feed.css",
  "/src/images/main-image.jpg",
  "https://fonts.googleapis.com/css?family=Roboto:400,700",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.3.0/material.indigo-pink.min.css"
];

function trimCache(cacheName, maxItems) {
  caches.open(cacheName)
    .then(cache => {
      return cache.keys()
      .then(keys => {
        if(keys.length > maxItems) {
          cache.delete(keys[0])
            .then(trimCache(cacheName, maxItems))
        }
      })
    })
}

self.addEventListener('install', event => {
  console.log('[Service Worker] Installing service worker', event);
  event.waitUntil(
    caches.open(CACHE_STATIC_NAME)
      .then(cache => {
        console.log("[Service Worker] Precaching App Shell");
        cache.addAll(STATIC_FILES);
      })
  );
});

self.addEventListener('activate', event => {
  console.log('[Service Worker] Activating service worker', event);
  event.waitUntil(
    caches.keys()
      .then(keyList => {
        return Promise.all(keyList.map(key => {
          if(key !== CACHE_STATIC_NAME && key !== CACHE_DYNAMIC_NAME) {
            console.log("[Service Worker] removing old cache", key);
            return caches.delete(key);
          }
        }))
      })
  );
});

function isInArray(str, arr) {
  for(var i=0; i<arr.length; i++) {
    if(arr[i] === str) {
      return true;
    }
  }
  return false;
}

// fetch event is called when requesting assets like css, js, image files
self.addEventListener('fetch', event => {
  var url = 'https://pwagram-123.firebaseio.com/posts.json';
  var staticAssets = [];
  if(event.request.url.indexOf(url) > -1) {
    event.respondWith(
      fetch(event.request)
        .then(res => {
          var clonedRes = res.clone();
          clearAllData('posts')
            .then(() => {
              return clonedRes.json();
            })
            .then(data => {
              for(var key in data) {
                writeData('posts', data[key]);
              }
            });
          return res;
        })
    );
  } else if(isInArray(event.request.url, STATIC_FILES)) {
    event.respondWith(
      caches.match(event.request)
    );
  } else {
    event.respondWith(
      caches.match(event.request)
        .then(response => {
          if(response) {
            return response;
          } else {
            return fetch(event.request)
              .then(res => {
                return caches.open(CACHE_DYNAMIC_NAME)
                        .then(cache => {
                          cache.put(event.request.url, res.clone());
                          return res;
                        });
              })
              .catch(err => {
                return caches.open(CACHE_STATIC_NAME)
                  .then(cache => {
                    if(event.request.headers.get('accept').includes('text/html')) {
                      return cache.match("/offline.html");
                    }
                  })
              });
          }
        })
    );
  }
});

self.addEventListener('sync', event => {
  console.log("Service worker ", event);
  if(event.tag === "sync-new-posts") {
    console.log("Service worker  syncing new post");
    event.waitUntil(
      readAllData('sync-posts')
        .then(data => {
          for (var dt of data) {
            fetch('https://pwagram-123.firebaseio.com/posts.json', {
              method: "POST",
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
              },
              body: JSON.stringify({
                id: dt.id,
                title: dt.title,
                location: dt.location,
                image: "https://firebasestorage.googleapis.com/v0/b/pwagram-123.appspot.com/o/sf-boat.jpg?alt=media&token=64779667-6ca5-4d52-8fd9-536b2aa69133"
              })
            })
              .then(res => {
                if(res.ok) {
                  res.json()
                    .then(resData => {
                      console.log("Send data ", resData);
                      deleteItemFromData('sync-posts', dt.id);
                    })
                }
              })
              .catch(err => {
                console.log("Error while sending data", dt.id);
              })
          }
        })
    );
  }
});